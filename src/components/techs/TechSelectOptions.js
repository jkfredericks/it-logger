import React, { useEffect } from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { getTechs } from '../../actions/techActions';

const TechSelectOptions = ({ onChange }) => {

  const dispatch = useDispatch()
  const techs = useSelector(state => state.tech.techs)
  const isLoading = useSelector(state => state.tech.loading)

  useEffect(() => {
    dispatch(getTechs())
  }, [dispatch]);

  // TEST 2: "Add Log Modal Select a Technician" 
  // 1.2. Complete the `TechSelectOptions` component to list all the technicians in a dropdown list.
  // First get the technicians, check if the endpoint has finished loading. 
  // Then map the technicians from state to create the options list with `firstName` and `lastName` values. 

  if (isLoading)
    return <div>Loading</div>

  return (
    <select onChange={onChange}>
      {techs.map((tech, index) => {
        return <option key={index} value={`${tech.firstName} ${tech.lastName}`}>{`${tech.firstName} ${tech.lastName}`}</option>
      })}
    </select>
  );
};

TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tech: state.tech,
});

export default connect(mapStateToProps, { getTechs })(TechSelectOptions);
